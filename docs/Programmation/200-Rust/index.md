# Rust - Bases

![Image title](https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg){ align=right }

Rust est un langage de programmation compilé multi-paradigme conçu et développé par Mozilla Research depuis 2010. Il a été conçu pour être « un langage fiable, concurrent, pratique » supportant les styles de programmation purement fonctionnel, modèle d'acteur, procédural, ainsi qu'orienté objet sous certains aspects.

En 2020, ses domaines de prédilection sont la programmation système, les applications en ligne de commande, les applications Web via WebAssembly, les services réseaux et les systèmes embarqués.

Du fait de la politique de Mozilla, Rust est entièrement développé de façon ouverte (les ingénieurs de Mozilla Research publient leurs idées et les décisions prises lors des réunions) et sollicite les remarques et contributions de la communauté. La conception du langage est graduellement améliorée au travers des retours de l'équipe travaillant sur le moteur de rendu Servo et de façon pragmatique lors de l'écriture du compilateur. Bien que le projet soit financé par Mozilla, la majorité des contributions proviennent de la communauté. 

Rust se veut un langage performant, sûr et productif.

Le langage peut notamment donner des garanties d'absence d'erreur de segmentation ou de situation de concurrence dès l'étape de compilation. De plus, ceci se fait sans ramasse-miettes. Ses performances sont comparables à celles de C ou C++ pour ce qui concerne la vitesse d'exécution.

Enfin, Rust est accompagné de Cargo, un gestionnaire de paquets permettant de gérer la compilation et les dépendances entre paquets. Le compilateur fournit des messages d'erreur explicites et utiles. Il existe aussi d'autres outils d'édition pour les EDI ainsi qu'une documentation abondante.

## Installation

Sur Linux/MacOS : `curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh` dans un terminal. Il vous faudra aussi installer un "linker". Les compilateurs C en contiennent habituellement un. Sur Arch, installer le paquet `base-devel` devrait suffire. Sur Ubuntu et dérivés, il faut installer le paquet `build-essential`. Pour MacOS, on peut installer un compilateur C avec `xcode-select --install`

Sous Windows, il faut aller sur https://www.rust-lang.org/tools/install et suivre les instructions pour installer Rust. À un moment donné durant l'installation, vous aurez un message vous expliquant qu'il va vous falloir l'outil de compilation MSVC pour Visual Studio 2013 ou plus récent. 

Pour les obtenir, il faudra installer [Visual Studio 2022](https://visualstudio.microsoft.com/downloads/) et choisir les charges de travail suivantes lorsque demandés : 

- "Développement Desktop en C++"
- Le SDK Windows 10 ou 11
- Le pack de language de votre choix.

## Apprendre le Rust

La communauté Rust a créé un livre officiel reprenant les bases du language. Ces guides sont largements basés dessus. Il est accessible [ici](https://doc.rust-lang.org/book/) (en anglais). Il existe une traduction francaise communautaire accessible [ici](https://jimskapt.github.io/rust-book-fr/)

## Hello, World!

Maintenant que Rust est installé, il est temps de créer un premier programme. On va commencer par le traditionnel "Hello, World!".

Débutons par créer un dossier pour stocker nos projets. Dans ce dossier, on va créer un fichier appelé `main.rs`.

On peut y écrire le code suivant : 

```rs
fn main() {
    println!("Hello, world!");
}
```

Ensuite on le compile dans un terminal avec `rustc main.rs` et on peut le lancer avec `./main` ou `./main.exe` en fonction de l'OS

## Hello, Cargo!

Cargo est le système de compilation et le gestionaire de paquets de Rust. La majorité des Rustacéens l'utilisent pour gérer leurs projets Rust parcequ'il gère plein de choses comme la compilation, le téléchargement des dépendances et leur compilations.

Les programmes les plus simples comme Hello, World! ne nécésitent pas de dépendances. Si nous avions utilisé Cargo, on ne l'aurait utilisé que pour le compilation du code. Au fur et a mesure de l'écriture de programes complexes, on ajoutera des dépendances, et ces ajouts seront beaucoup plus facile grace a Cargo.

Crééons un projet avec Cargo et observons les différences avec notre simple Hello World.

On peut créer un projer avec `cargo new <projet>`. Le projet qu'on va créer va s'appeler hello_cargo

Cela va créer un dossier hello cargo contenant la configuration du projet et un repo git. La configuration du projet se fait dans le fichier `cargo.toml`. Il devrait ressembler a ceci :

```toml
[package]
name = "hello_cargo"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
```

La section `[package]` permet de définir les informations du programme comme le nom du paquet ou la version. La section `[dependencies]` permet de lister les dépendances nécessaires a la compilation et l'exécution du programme.

Ouvrons maintenant le fichier `src/main.rs`. Cargo aura généré un script Hello, World pour nous.

Cargo attend que les fichiers sources soient placés dans le dossier src. Le dossier parent nous sert pour des Reamde, des informations sur la license et tout ce qui n'est pas lié au code.

Lorsqu'on a créé un projet sans utiliser cargo, comme au début, on peut le convertir en projet cargo en placant le code dans le dossier `src` et en créant le fichier `cargo.toml` adéquat.

Maintenant on peut compiler le programme en exécutatnt `cargo build`. La commande va créer un exécutable dans `target/debug/hello_cargo`. Étant donné que la compilation de base est une compilation de déboguage (debug en anglais), l'exécutable est stocké dans le dossier `debug`. Si tout va bien, l'exécutable compilé devrait afficher "Hello, world!" dans un terminal. La première compilation va générer un fichier Cargo.lock qui traque les versions des dépendances. Il ne sera pas nécessaire de changer le fichier a la main, Cargo s'en chargera.

On peut également utiliser `cargo run` pour compiler et exécuter automatiquement le programme. Enfin, la commande `cargo check` permet de vérifier rapidement si il n'y a pas d'erreurs dans le programme

Lorsque le programme est enfin prêt pour la publication, on peut le compiler avec `cargo run --release`. L'exécutable produit sera beaucoup plus rapide a exécuter car il a été optimisé au cours de la compilation. Cependant, la compilation sera plus longue. C'est pour cela que le profil de déboguage est présent, car il est plus rapide a compiler ce qui le rend plus facile a tester et corriger.