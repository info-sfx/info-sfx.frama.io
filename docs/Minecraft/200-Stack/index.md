# Stack technique

Bon, si vous êtes la, c'est que vous voulez plus de détails a propos du serveur. Bien. Vous êtes courageux. Voici donc des notes à ce propos, flemme de faire la mise en forme

Version de Minecraft : Forge 1.12.2

Logiciel utilisé pour le serveur : [Magma](https://magmafoundation.org/)

Dans les listes ci dessous, les objets placés sur un niveau inférieur sont des dépendances.

Liste des plugins :

- Bessentials
- BuildBattle
    - ActionBar API
- ClearLag
- Legend lootboxes
- MultiverseCore
- MultiversePortals
- PVP1vs1
- SkinsRestorer
- WorldEdit
- WorldGuard

Liste des mods : 
- Tinkers Construct
    - Mantle
- Construct's Armory
- Thermal Foundation
    - CoFH Core
    - RedstoneFlux
    - CoFH World
- Chisel
    - ConectedTextureMod (Client Only)
- Optifine (Client Only)
- Just Enough Items (MultiMC Only)
- Optifine (MultiMC Only)