# Burp Suite

Burp Suite est une application Java, développée par PortSwigger Ltd, qui peut être utilisée pour la sécurisation ou effectuer des tests de pénétration sur les applications web. La suite est composée de différents outils comme un serveur proxy (Burp Proxy), robot d’indexation (Burp Spider), un outil d'intrusion (Burp Intruder), un scanner de vulnérabilités (Burp Scanner) et un répéteur HTTP (Burp Repeater).

## Module tryhackme

https://tryhackme.com/room/burpsuitebasics

## Installation

Linux/MacOS/Windows :

Télécharger l'installateur approprié provenant de ce [lien](https://portswigger.net/burp/releases/professional-community-2022-8-5?requestededition=community)

Arch Linux : 

Burp Suite est disponible via le AUR. Installez le paquet `burpsuite`

## Comment démarrer le logiciel ?

Lancer Burp Suite CE (Community Edition), et garder les paramettres par défaut.

## Utilisation

Cette section va présenter les différentes utilisations de Burp, car il est impossible pour moi de penser à une situation ou on fait toujours la même chose.

### Target

Cet onglet affiche des informations a propos du contenu de la page que vous visitez via le navigateur intégré.

### Proxy

Cet onglet va vous permetre d'intercepter des paquets et de les envoyer dans différents autres onglets, ou de les modifier avant de les envoyer.

### Intruder

Cet onglet permet de configurer une attaque automatique en envoyant la même requêtte plein de fois avec une ou des variables qui changent a chaque reqêtte

### Repeater

Permet de modifier et de re-envoyer une requête



Je ne juge pas nécessaire de détailler les autres catégories car elles ne nous serons surement pas utilesw