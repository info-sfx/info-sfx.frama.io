# Liens et ressources utiles

TryHackMe : Site de challenges et de cours de cybersécurite, avec les contenus basiques gratuits : [Lien](https://tryhackme.com)

root-me : Site de challenges : [Lien](https://root-me.org)

HackTheBox : Site de challenges : [Lien](https://hackthebox.eu)

Exploit-DB : [Lien](https://www.exploit-db.com/)

CERT-FR : Centre gouvernemental de veille, d'alerte et de réponse aux attaques informatiques : [Lien](https://www.cert.ssi.gouv.fr/)