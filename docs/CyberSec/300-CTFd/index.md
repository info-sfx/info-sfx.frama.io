# CTFd

CTFd : Des CTF selons vos besoins.

CTFd est un framework Capture The Flag axé sur la facilité d'utilisation et la personnalisation. Il est livré avec tout ce dont vous avez besoin pour lancer un CTF et il est facile à personnaliser avec des plugins et des thèmes.

![](../../pictures/CTFd-SS.png)

Une démo est disponible [ici](https://demo.ctfd.io)

## Installation 

Il est possible d'installer grace a python ou l'image docker si on connait le système

### Python

Après avoir récupéré les sources, on installe les dépendances avec `pip install -r requirements.txt`. On peut ensuite modifier le fichier de configuration appelé `config.ini` selon nos besoins. Enfin, on utilise `python3 serve.py` pour lancer CTFd

### Docker

`docker run -p 8000:8000 -it ctfd/ctfd`

### Docker compose

`docker-compose up` dans le dossier contenant les sources