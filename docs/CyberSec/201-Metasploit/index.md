# Metasploit

![Capture d'écran de Metasploit Framework](../../pictures/metasploit/Metasploit.png){ align=right }

Metasploit ou Metasploit Pen Testing Tool est un projet (open source, sous Licence BSD modifiée) en relation avec la sécurité des systèmes informatiques. Son but est de fournir des informations sur les vulnérabilités de systèmes informatiques, d'aider à la pénétration et au développement de signatures pour les systèmes de détection d'intrusion (IDS, Intrusion Detection System).

Le plus connu des sous-projets est le Metasploit Framework, un outil pour le développement et l'exécution d'exploits (logiciels permettant d'exploiter à son profit une vulnérabilité) contre une machine distante. Les autres sous-projets importants sont la base de données d'Opcode, l'archive de shellcode, et la recherche dans la sécurité.

Créé à l'origine en langage de programmation Perl, Metasploit Framework a été complètement réécrit en langage Ruby. Le plus notable est la publication de certains des exploits les plus techniquement sophistiqués auprès du public. C'est un outil très puissant pour les chercheurs en sécurité travaillant sur les potentielles vulnérabilités de systèmes informatiques. 

## Installation

Sous Linux/MacOS

Dans un terminal : 

```bash
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall
chmod 755 ./msfinstall
./msfinstall
```

Sous Windows 

Télécharger l'installateur avec ce [lien](https://windows.metasploit.com/metasploitframework-latest.msi) (Téléchargement direct), et régler votre antivirus en conséquence

## Lancement

Dans un terminal, executez `msfconsole`

Au premier lancement, initialisez la base de données en répondant `yes` à la première question

## Utilisation

Après avoir lancé msfconsole, vous serez dans un "shell" metasploit, signalé par le prompt `msf6 >`

Dans ce shell, on peut chercher un module avec la commande `search`

On peut ensuite utiliser ces résultats pour sélectionner rapidement un module avec `use <numéro du module>`

Il est nécessaire ensuite de définir les options requises pour le bon fonctionnement du module. On peut afficher ces options grace à la commande `set <option> <valeur>`

Enfin, pour lancer l'exploir, il suffit juste de lancer `exploit`