# nmap

## Installation

Linux : Dans la majorité des repos officiels, paquet `nmap`

Windows : Téléchargez l'installateur via le site officiel, démerdez vous j'ai écrit cette page a 23h45 dans ma voiture sans co

## Utiliation

Impossible de faire une liste exhaustive ici, donc voila ce que j'utilise

`nmap -sV <adresse> -vvv` : Scanne les services de adresse. -vvv permet de mettre la verbose au maximum  
`nmap -sV <adresse> -p<port du début>-<port de fin> -vvv` Permet de scanner les services de addreses qui utilisent les ports spécifiés. Ecrire `-p-`va scanner tous les ports de la machine.
`nmap -sV <adresse> -vvv -Pn` : Scanne les services de la machine cible mais désactive le ping de test pour savoir si la machine est en marche

Toutes ces options combinées permettent donne `nmap -Sv <adresse> -p- -Pn`

Pour plus d'infos, se réferrer a la room TryHackMe