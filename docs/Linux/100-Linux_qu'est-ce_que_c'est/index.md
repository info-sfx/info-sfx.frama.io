# Linux, qu'est ce que c'est ?

## Noyau Linux

Le noyau Linux est un noyau de système d'exploitation de type UNIX. Il est utilisé dans plusieurs systèmes d'exploitation dont notamment GNU/Linux (couramment appelé « Linux ») et Android. Le noyau Linux est un logiciel partiellement libre (contenant des BLOB et des modules non-libre - consultez Linux-libre) développé essentiellement en langage C par des milliers de bénévoles et salariés collaborant sur Internet. 

???+ info "Noyau"
    Le noyau est le cœur du système, c'est lui qui s'occupe de fournir aux logiciels une interface de programmation pour utiliser le matériel.

Le noyau Linux a été créé en 1991 par Linus Torvalds pour les compatibles PC. Ses caractéristiques principales sont d'être multitâche et multi-utilisateur. Il respecte les normes POSIX ce qui en fait un digne héritier des systèmes UNIX. Au départ, le noyau a été conçu pour être monolithique. Depuis sa version 2.0, le noyau, bien que n'étant pas un micro-noyau, est modulaire, c'est-à-dire que certaines fonctionnalités peuvent être ajoutées ou enlevées du noyau à la volée (en cours d'utilisation). 

## GNU/Linux

Linux ou GNU/Linux est une famille de systèmes d'exploitation open source de type Unix fondé sur le noyau Linux, créé en 1991 par Linus Torvalds. De nombreuses distributions Linux ont depuis vu le jour et constituent un important vecteur de popularisation du mouvement du logiciel libre.

Si à l'origine, Linux a été développé pour les ordinateurs compatibles PC, il n'a jamais équipé qu'une très faible part des ordinateurs personnels. Mais le noyau Linux, accompagné ou non des logiciels GNU, est également utilisé par d'autres types de systèmes informatiques, notamment les serveurs, téléphones portables, systèmes embarqués ou encore superordinateurs. Le système d'exploitation pour téléphones portables Android qui utilise le noyau Linux mais pas GNU, équipe aujourd'hui 85 % des tablettes tactiles et smartphones. 

À l'origine, le terme Linux ne désignait que le noyau de système d'exploitation Linux. Puis, par métonymie, l'usage du terme Linux s'est répandu pour décrire tant le noyau Linux que l'ensemble du système d'exploitation, incluant les programmes GNU. le nom GNU/Linux a été initié par le projet Debian, pour créditer à la fois les développeurs de GNU (l'OS) et de Linux (le noyau). Cependant, le terme Linux reste le plus répandu. Le principal argument des promoteurs de cette appellation est l'argument de simplicité : « Linux » est plus court à écrire et prononcer que « GNU/Linux ».

## Distributions

Une distribution Linux, appelée aussi distribution GNU/Linux lorsqu'elle contient les logiciels du projet GNU, est un ensemble cohérent de logiciels, la plupart étant des logiciels libres, assemblés autour du noyau Linux, et formant un système d'exploitation pleinement opérationnel.

Le terme « distribution » est calqué sur l’anglais software distribution qui signifie « collection de logiciels » en français.

Il existe une très grande variété de distributions Linux, chacune ayant des objectifs et une philosophie particulière. Les éléments les différenciant principalement sont : la convivialité (facilité de mise en œuvre), l'intégration (taille du parc de logiciels validés distribués), la notoriété (communauté informative pour résoudre les problèmes), leur fréquence de mise à jour, leur gestion des paquets et le mainteneur de la distribution (généralement une entreprise ou une communauté). Leur point commun est le noyau Linux, et un certain nombre de commandes Unix. 


<br><br><br>
Merci wikipedia