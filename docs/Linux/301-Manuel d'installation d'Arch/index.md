# Installation d'Arch

Cette page va servir de manuel d'installation de Arch, même si on ne va pas le faire mais pour référence ultérieure.

Il y a déja un manuel d'installation proposé sur le wiki à [cette adresse](https://wiki.archlinux.org/title/Installation_guide_(Fran%C3%A7ais)). Cependant ce guide peut être difficile a apréhender pour les nouveaux utilisateur. Ce guide se veut donc plus clair et plus agréable a regarder.

!!! warning "Disclaimer"
    Je ne suis pas responsable de toute perte de données liée entierrement ou en partie a l'utilisation de ce guide.

## Préparation avant l'installation

### Obtention de l'image d'installation

On peut télécharger l'iso d'Arch Linux sur leur site, via un torrent ou un mirroir. Les liens de téléchargement sont disponibles [ici](https://archlinux.org/download/)

??? info "Vérifier la signature"
    Il est possible de vérifier la signature de l'image mais cela n'est pas nécessaire, je le fais jamais. Pour les interresés, cliquez [ici](https://wiki.archlinux.org/title/Installation_guide_(Fran%C3%A7ais)#V%C3%A9rifier_la_signature)

### Démarer l'iso

Pour démarer l'iso, il faut d'abbord la graver sur une clé USB, sur un CD ou via PXE si c'est votre délire.

L'image va démarer sur GRUB si l'iso est démaré avec un UEFI ou syslinux avec un BIOS

!!! warning "Secure Boot"
    L'iso d'installation ne prend pas en charge le Secure Boot, qui doit être désactivé afin de pouvoir démarrer le support d'installation.

### Disposition de clavier

La disposition par défaut est US. Pour passer à la disposition française, il faut exécuter `loadkeys fr-latin1`

### Vérifier le mode de démarage

Pour connaître le «mode de démarrage», consultez le répertoire efivars: 

```bash
ls /sys/firmware/efi/efivars
```

Si la commande affiche le répertoire sans erreur, alors le système est démarré en mode UEFI. Si le répertoire n’existe pas, le système a probablement démarré en mode BIOS (ou CSM). Si le système n'a pas démarré dans le mode souhaité, consultez le manuel de votre carte mère. 

### Connection a Internet

Pour configurer la connexion réseau dans l'image live, suivez ces étapes :

- Vérifiez que votre Carte réseau est répertoriée et activée, par exemple avec ip-link(8):  
 `ip link`

- Pour les connections sans fil comme le Wi-Fi, vérifiez que l'utilitaire rfkill ne bloque pas l'interface.  
  Ethernet : Connectez le câble.  
  Wi-Fi : Authentifiez-vous sur le réseau sans fil avec iwctl.

- DHCP: l'adressage dynamique des adresses IP et des serveurs DNS devraient fonctionner par défaut pour les protocoles Ethernet et Wi-Fi. Si ce n'est pas le cas, il est possible d'exécuter `dhcpcd`

### Partitionnement des disques

Une fois reconnus par le système live, les disques se verront affectés un périphérique de type bloc tel que /dev/sda, /dev/nvme0n1 ou /dev/mmcblk0. Pour identifier ces périphériques, utilisez lsblk ou fdisk. 

`fdisk -l `

Les résultats se terminant par: rom, loop ou airoot peuvent être ignorés. 

Les partitions suivantes sont nécessaires sur un périphériques choisi: 
- Une partition racine `/`
- Une partition EFI afin de pouvoir démarer en mode UEFI

#### Examples de partitionnement : 

1 - UEFI avec GPT

| Point de montage | Type de partition    | Taille sugérée     |
|------------------|----------------------|--------------------|
| `/mnt/boot`      | EFI system partition | Au moins 300Mo     |
| [SWAP]           | Linux Swap           | Plus de 512Mo      |
| `/mnt`           | Linux Filesystem     | Le reste du disque |

2 - BIOS avec MBR

| Point de montage | Type de partition | Taille recomandée  |
|------------------|-------------------|--------------------|
| [SWAP]           | Linux Swap        | Plus de 512Mo      |
| `/mnt`           | Linux Filesystem  | Le reste du disque |


#### Partitionnement

!!! danger "Avertissement"
    Ecrire la table de partition va détruire toutes les donées présentes sur le disque. Sauvegardez les bien avant de continuer

On utilise fdisk pour modifier la table de partition : 

`fdisk /dev/dique`

Utilisation de fdisk :

- g : convertit le disque en mode GPT
- n : créée une nouvelle partition
- t : change le type d'une partition
- d : supprime une partition
- w : Ecrit la table de partition sur le disque

Commandes a utiliser dans le 1er cas : 

- `g` : Convertit le dique en GPT
- `n` : Démarrer la création d'une partition
- `1` : 1ère partition
- `<Entrée>` : Utiliser la valeur par défaut pour le premier secteur
- `+300M` : Définir le dernier secteur a celui 300Mo plus loin que le secteur de début
- `t` : Changer le type de la partition
- `<Entrée>` : Choisir la première partition
- `1` : Choisir le type EFI
- `n` : Démarrer la création d'une partition
- `2` : 2ème partition
- `<Entrée>` : Utiliser la valeur par défaut pour le premier secteur
- `+2G` : Définir le dernier secteur a celui 2Go plus loin que le secteur de début
- `t` : Changer le type de la partition
- `<Entrée>` : Choisir la deuxième partition
- `19` : Choisir le type Linux swap
- `n` : Démarrer la création d'une partition
- `3` : 3ème partition
- `<Entrée>` : Utiliser la valeur par défaut pour le premier secteur
- `<Entrée>` : Définir le dernier secteur de la partition comme le dernier secteur du disque
- `w` : Ecrire la table sur le disque

J'utilise ce schéma sur un disque de 20 a 40 Go dans une machine virtuelle.

Commandes a utiliser dans le 2ème cas : 

Il est inutile de convertir le disque en MBR car il sera créé dans ce mode par défaut. Si jamais il faut le convertir, la commande a utiliser est `o`

- `n` : Démarrer la création d'une partition
- `1` : 1ère partition
- `<Entrée>` : Utiliser la valeur par défaut pour le premier secteur
- `+2G` : Définir le dernier secteur a celui 2Go plus loin que le secteur de début
- `t` : Changer le type de la partition
- `<Entrée>` : Choisir la deuxième partition
- `19` : Choisir le type Linux swap
- `n` : Démarrer la création d'une partition
- `2` : 2ème partition
- `<Entrée>` : Utiliser la valeur par défaut pour le premier secteur
- `<Entrée>` : Définir le dernier secteur de la partition comme le dernier secteur du disque
- `w` : Ecrire la table sur le disque
 

A partir de maintenant, dans la table UEFI il y a /dev/sda1 qui est la `partition_efi`, /dev/sda2 la `partition_d'échange` et /dev/sda3 la `partition_racine` et dans la table BIOS /dev/sda1 qui est la `partition_d'échange` et /dev/sda2 la `partition_racine`. Il faudra remplacer le nom de la partition par le périphérique de type bloc correspondant.

!!! note
    La partition d'échange est recomandée mais n'est 

### Formatage de partition

Pour la partition racine : 

`mkfs.ext4 /dev/partition_racine`

Pour le swap, le cas échéant :

`mkswap /dev/partition_d'echange`

Partition UEFI, le cas échéant : 

`mkfs.vfat -F 32 /dev/partition_EFI`

!!! danger
    Ne formatez la partition système EFI que si vous l'avez créée pendant le partitionnement. S'il y avait déjà une partition système EFI sur le disque précédemment, son formatage peut détruire les chargeurs d'amorçage des autres systèmes d'exploitation installés.

### Montage des systèmes de fichiers

Partition racine : 

`mnt /dev/partition_racine /mnt`

Partition d'échange, le cas échéant : 

`swapon /dev/partition_d'échange`

Partition UEFI, le cas échéant : 

`mnt --mkdir /dev/partition_EFI /mnt/boot`

## Installation

### Sélection des mirroirs

Les paquets à installér doivent être téléchargés depuis les miroirs des dépôts officiels tels que définis dans /etc/pacman.d/mirrorlist. Sur le système live, après connexion à Internet, reflector met à jour la liste des miroirs en sélectionnant les 20 miroirs les plus récemment synchronisés et en les triant par vitesse de téléchargement. Plus le miroir se trouve haut dans la liste, plus grande est sa priorité lors d'un téléchargement. Vérifiez le contenu de /etc/pacman.d/mirrorlist et modifiez le si besoin. Placez les miroirs les plus proches géographiquement en haut de la liste, bien que d'autres critères sont à prendre en compte.

Par exemple pour trouver un miroir plus proche de chez vous (et/ou plus rapide) en utilisant reflector : 

`reflector --country France --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist`

Cette commande devrait sélectionner les miroirs HTTPS synchronisés ces 12 dernières heures en France, les trier par vitesse de téléchargement, et mettre à jour le fichier /etc/pacman.d/mirrorlist.

pacstrap copiera plus tard ce fichier sur le nouveau système, prendre votre temps sur cette étape peut donc vous en faire gagner plus tard. 

### Instalation des paquets essentiels

Utilisez le script pacstrap(8) en lui indiquant le répertoire correspondant à la racine du système suivi des paquets ou groupes à installer (séparés par un espace). Pour le système de base, installez le "méta-paquet" base, le noyau, et le microprogramme pour les périphériques les plus courants: 

`pacstrap -K /mnt base linux linux-firmware`

Il faut ajouter d'autres paquets car le paquet base ne comprent pas tous les paquets de l'iso

- `iwd` pour la connection a Internet
- `dhcpcd` pour le serveur DHCP

Ces deux paquets sont nécessaires pour l'instant mais pourront être remplacés au moment de l'installation de l'environnement du bureau

- `nano`, éditeur de texte pour créer les fichiers de configuration
- `man-db`, `man-pages` et `texinfo` pour la documentation des paquets.

## Configuration du système

### Fstab

Il faut générer le fichier fstab pour pouvoir faire marcher l'installation d'Arch

`genfstab -U /mnt >> /mnt/etc/fstab`

### Chroot

Chrooter signifie *change root*, ce qui va permettre d'exécuter toutes les prochaines commandes dans la nouvelle installation d'Arch

### Fuseau Horaire

Pour définir le fuseau horaire, par exemple celui de la France, on exécute

`ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime`

Il faut ensuite exécuter `hwclock --systohc`

### Locales

Modifiez /etc/locale.conf avec `nano /etc/locale.gen`. Cherchez la ligne avec fr_FR.UTF-8 et décomentez la en enlevant le `#`. Sauvegardez le fichier et exécutez `locale-gen`. Ensuite, il faut créer le fichier `/etc/locale.conf` et définir la variable LANG a l'intérieur. Il suffit d'écrire `LANG=fr_FR.UTF-8`. Cela passera la langue du système en français au prochain redémarage. Ensuite, il faut modifier le fichier `/etc/vconsole.conf` afin de spécifier la disposition de clavier a utiliser. Pour la disposition AZERTY, il suffit d'écrire dans le fichier `KEYMAP=fr-latin1`

### Configuration réseau

Tout d'abbord, il faut créer le fichier `/etc/hostname` et y écrire le nom de la machine. 

Pour configurer le réseau, il faudra activer le serveur DHCP. executez `systemctl enable dhcpcd.service`. Si votre ordinateur est connecté via un cable Ethernet, cela devrait marcher. Pour le Wi-Fi, activez iwd avec `systemctl start iwd.service`. Il faudra ensuite configurer le réseau une fois le système redémaré comme au début de l'installation. 

### Initramfs

La création d'un nouvel initramfs n'est généralement pas nécessaire, car mkinitcpio a été lancé lors de l'installation du noyau avec pacstrap. 

Mais vous pouvez toujours le faire avec `mkinitcpio -P`. Je le fait a chaque installation, on sait jamais.

### Mot de passe administrateur

Définissez un mot de passe pour root avec passwd

## Installation du chargeur d'amorçage

Dans ce guide, nous allons installer GRUB

!!! info "Installer le microcode"
    Si vous avez un processeur de la marque AMD ou Intel, il faut installer le microcode, dans lequel le système ne sera pas démarrable. Il est possible de l'installer avec `pacman -S amd-ucode` ou `pacman -S intel-ucode`. GRUB le détectera automatiquement.

### Installation pour les systèmes UEFI

Tout d'abbord, il faut installer les paquets `grub` et `efibootmgr`

Ensuite, il faut exécuter `grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=GRUB`

### Installation pour les systèmes BIOS

Tout d'abbord, il faut installer le paquet `grub`

Ensuite, il faut exécuter `grub-install --target=i386-pc /dev/sda`

### Génération de la configuration

Pour générer la configuration, on peut utiliser l'outil `grub-mkconfig`

Il faut exécuter `grub-mkconfig -o /boot/grub/grub.cfg`

## Finalisation

On peut maintenant sortir de l'environnement chroot en tappant exit. on peut "éjecter" les partitions avant de redémarrer avec `umount -R /mnt` Si vous avez activé la partition d'échange avec `swapoff /dev/partition_d'échange`. On finit par redémarer la machine avec `reboot`.

Félicitations, vous avez installé Arch Linux