# Git

Git est un gestionnaire de version de fichier

## Installation

Linux : Disponible dans les repos officiels, paquet git

Windows : Télécharger [l'installateur](https://git-scm.com/download/win) ou via winget

MacOS : Disponible via brew et macports.

## Utilisation

Traduit et adapté depuis gittutorial

Accrochez vous, fort de préférence, ca risque d'être long

### Créer un repo vide

`git init` dans le dossier contenant les fichier a mettre dans un repo

Cela va créer un dossier .git dans le répertoire courrant, qui va stocker toutes les informations sur les changements apportés aux fichiers

### Faire des changements

Pour que git enregistre les changements, il faut tout d'abbord ajouter les fichers au dépot. `git add .` permet d'ajouter tous les fichiers et dossiers dans un index qui est temporaire. Pour ajouter cet index, il faut exécuter `git commit`. Cette commande vous demandera un message de commit, et l'index sera donc stocké dans le dépot.

Une fois que vous avez modifié des fichier, on peut les ajouter à l'index grâce a `git add fichier1 fichier2 fichier3`. On peut voir ce qui est sur le point d'être commité grace a `git diff --cached`. le `--cached` perment de voir seulement les changements qui sont indexés. On peut avoir un résumé de la situation de dépot avec `git status`.

S'il y a besion de faire d'autres changements, on peut le faire maintenant en oubliant pas d'ajouter les modifications a l'index.

On peut maintenant commit avec `git commit`, ce qui demandera a nouveau un message de commit

### Voir l'historique du projet

On peut voir a tout moment l'historique des changements grace à `git log`. On peut voir les diff a chaque étape grace à `git log -p`. On peut avoir un résumé avec `git log --stat --summary`

### Gestion des branches

Un dépot git peut contenir plusieurs branches de développement.

Pour créer une nouvelle branche, on peut utiliser `git branch nouvelle_branche`

Maintenant, on peut afficher la liste des branches grâce a `git branch` et on peut voir qu'il y a maintenant les 2 branches. Celle précédée par une étoile est la branche actuelle. on peut changer de branche avec `git switch nouvelle_branche`. On peut maintenant modifier un fichier et le commit. Si on retourne sur la branche principale avec `git switch master`, on peut voir que les changement effectués ne sont plus la.

Dans l'état acctuel, les branches ont divergées. Pour fusionner (merge) les changement effectués sur la nouvelle branche dans la branche principale, on peut exécuter `git merge nouvelle_branche`

`gitk` permet d'avoir une représentation graphique des changements. L'extension Git Graph sur VSCode le permet également.

On peut supprimer une branche avec `git branch -d nouvelle_branche`. Cette commande va s'assurer que tout les changements apportés dans la branche le sont également dans la branche courante. Si vous avez eu une idée mais que vous regrettez, on peut supprimer la branche avec `git branch -D idee_nulle`