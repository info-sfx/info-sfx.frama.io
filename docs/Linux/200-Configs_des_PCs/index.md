# Config des PCs

Sur les PCs, la distribution EndeavourOS est installée. Cette distribution est basée sur Arch Linux qui est beaucoup plus compliquée a installer et à utiliser. Endeavour est une solution "clé en main".

<figure markdown>
  ![EndeavourOS](../../pictures/endeavour.png)
  <figcaption>Capture d'écran d'EndeavourOS</figcaption>
</figure>

L'environnement de bureau va différer de la capture d'écran : Dans la capture d'écran, l'environnement de bureau est XFCE, mais celui qu'on utilisera est KDE Plasma

<figure markdown>
  ![KDE Plasma](../../pictures/KDE-Plasma.png)
  <figcaption>Capture d'écran d'EndeavourOS</figcaption>
</figure>