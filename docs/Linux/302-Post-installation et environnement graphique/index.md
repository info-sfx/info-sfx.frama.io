# Post-installation

Une fois votre installation d'Arch terminée, il y a plusieurs choses possibles

## Créer un nouvel utilisateur

Pour créer un nouveau compte, on exécute `useradd -m <username>`. On définit le mot de passe avec `passwd <username>`

## Accès au AUR (Arch User Repository)

Le dépôt des utilisateurs d'Arch (Arch User Repository) est un dépôt communautaire pour les utilisateurs d'Arch. Il contient des descriptions de paquets (PKGBUILDs) qui vous permettent de compiler un paquet à partir des sources avec makepkg puis de l'installer via pacman. L'AUR a été créé pour organiser et partager les nouveaux paquets de la communauté et pour aider à accélérer l'inclusion des paquets populaires dans le dépôt de la communauté. Cette section explique comment les utilisateurs peuvent accéder à l'AUR

!!! warning "Attention"
    Les paquets AUR sont des contenus produits par les utilisateurs. Ces PKGBUILDs sont complètement non-officiels et n'ont pas été vérifiés de manière approfondie. Toute utilisation des fichiers fournis est à vos propres risques.

Dans ce guide, nous allons installer yay (Yet Another Yaourt)

### Prérequis

Il faut tout d'abbord installer les outils de développement et git. On peut le faire avec la commande `pacman -S --needed base-devel git`.

### Installation

On peut récupérer le fichier binaire de yay grace à `git clone https://aur.archlinux.org/yay-bin.git`

On va ensuite dans le dossier `yay-bin` et exécuter `makepkg -si` qui va installer le paquet

Le AUR va vous permettre d'accéder a de nombreux paquets en plus de ceux qui sont présent dans les repos officiels.

## Environnement de bureau

Cette section va expliquer comment installer un environnement de bureau, pour une utilisation plus agréable d'Arch.

Les environnement disponibles sont listés [ici](https://wiki.archlinux.org/title/Desktop_environment#List_of_desktop_environments)

Cette section va se pencher sur les cas de GNOME et de KDE

### GNOME

Tous les logiciels fournis avec Gnome sont disponible dans le groupe `gnome`. Il y a aussi un groupe `gnome-extra` qui inclut également un client mail, IRC, GNOME tweaks et quelques jeux.

Pour démarer GNOME automatiquement, il suffit d'activer le gestionnaire de connection `gdm`. Pour cela on active le service `gdm` avec `systemctl start gdm.service`

### KDE

Pour installer KDE, il faut avoir installé X.org au préalable. Il est installable grace a `xorg-server`. Pour installer KDE, il faut installer le groupe `plasma`. Pour installer la suite d'application KDE, il est possible d'installer le groupe `kde-applications`.

KDE n'a pas de gestionnaire d'affichage inclus mais il y en a un recommandé, qui est `sddm`. Il est installable avec le paquet du même nom. On peut le lancer au démarrage en activant le service sddm avec `systemctl enable sddm.service`

