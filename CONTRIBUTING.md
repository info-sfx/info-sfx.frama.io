# Guide de contribution

Normalement, il y a que moi qui ai a modifier le backend. Personne ne push sur le repo dirrectement sauf moi, et tout le monde travaille sur son fork autrement et ouvre une PR avec ses modifications.

Le contenu est rédigé en Markdown. N'ajoutez pas de catégories dans la navbar. Vous n'avez pas le droit de modifier l'ordre des dossiers déja présents. Par exemple, dans la catégorie Linux, le numéro devant le dossier doit être suppérieur a 400.

Si vous souhaitez modifier le backend, merci de faire des PR séparées pour du contenu et pour modifier le backend.

Gardez les contribution dans le propos de ces ressources

Toute PR qui ne remplit pas ces critères se verra refusée.

Si un fichier n'est pas au bon endroit, je me résèrve le droit de le déplacer

Le site est actualisé tout les jours à minuit sauf dans le cas d'une modification urgente

## Préparation de l'environnement de développement

### Prérequis

- Avoir installer git
- Avoir installer python 3 et pip
- Avoir un éditeur de texte

### Installations des dépendances

- Cloner le repo
- Ouvrir un terminal dans le dossier et executer `pip install -r requirements.txt`

### Lancer le serveur

Pour lancer le serveur et voir vos modifications en direct, ouvrez un terminal dans le dossier et lancez la commande `mkdocs serve`, ce qui lancera un serveur web. Il est possible d'accéder a la page grâce au lien écrit dans la console ou via l'adresse https://localhost:8000/equipe-info, si aucune modification a la configuration a été effectuée.